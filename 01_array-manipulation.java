import java.util.Arrays;

public class array_manipulation
{
    static int find_index(int array[],int incremented[])
    {
        for(int i = 0 ; i < array.length-1;i++)
        {
            if(array[i+1]-array[i]==1 && incremented[i]==0)
            {
                return i;
            }
        }
        return -1;
    }    
    public static void main(String[] args)
    {
        int array[] = {6, 4, 2, 1, 3};
        int q = 7;
        
        if(q>array.length)
            q=array.length;
        
        int incremented[] = new int[array.length];
        Arrays.sort(array);
        
        int count=0;
        for(int i = 0 ; i < array.length ; i++ )
        {
            int index = find_index(array,incremented);
            if(index!=-1)
            {
                array[index]++;
                incremented[index]++;
                count++;
            }
            else
            {
                break;
            }
        }
        System.out.println(count);
    }
}

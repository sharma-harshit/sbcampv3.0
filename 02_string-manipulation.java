
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;



public class string_manipulation
{
    static HashSet<String> st = new HashSet<>();
    static boolean check_palindrome(String s)
    {
        for(int i = 0 ; i < s.length(); i++)
        {
            if(s.charAt(i)!= s.charAt(s.length()-i-1))
            {
                return false;
            }
        }
        return true;
    }
    static void subsequence(String str)
    {
        
        for (int i = 0; i < str.length(); i++) {
             
            for (int j = str.length(); j > i; j--) {
                String sub_str = str.substring(i, j);
             
                if (!st.contains(sub_str))
                    st.add(sub_str);
 
                for (int k = 1; k < sub_str.length() - 1; k++) {
                    StringBuffer sb = new StringBuffer(sub_str);
 
                    sb.deleteCharAt(k);
                    if (!st.contains(sb))
                        ;
                    subsequence(sb.toString());
                }
            }
        }
    }
    public static void main(String[] args)
    {
        String s="BBACC";
        subsequence(s);
        ArrayList<String> al = new ArrayList();
        for(String str  : st)
        {
            if(check_palindrome(str))
            {
                al.add(str);
            }
        }
        Collections.sort(al);
        for(String string : al)
        {
            System.out.println(string);
        }
    }
}
